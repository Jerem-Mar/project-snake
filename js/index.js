canvas = document.getElementById('gameCanvas');
ctx = canvas.getContext('2d');

let width = canvas.width;
let height = canvas.height;
let scale = 20;

let snakeState = {
    snakeX: width/2,
    snakeY: height/2,
    pressedKeys: {
        left: false,
        right: false,
        up: false,
        down: false
    }
};

let food = {
  foodX: 0,
  foodY: 0
};

function update(speed) {
/*snake update*/
    if (snakeState.pressedKeys.left) {
        snakeState.snakeX -= speed;
        if (snakeState.snakeX < 0) {
            snakeState.snakeX = 0
        }
    }

    if (snakeState.pressedKeys.right) {
        snakeState.snakeX += speed;
        if (snakeState.snakeX >= width-scale) {
            snakeState.snakeX = width-scale
        }
    }

    if (snakeState.pressedKeys.up) {
        snakeState.snakeY -= speed;
        if (snakeState.snakeY < 0) {
            snakeState.snakeY = 0
        }
    }

    if (snakeState.pressedKeys.down) {
        snakeState.snakeY += speed;
        if (snakeState.snakeY >= height-scale) {
            snakeState.snakeY = height-scale
        }
    }
/*end snake update*/

}

function draw() {
    ctx.clearRect(0, 0, width, height);
    ctx.fillStyle = 'black';
    ctx.fillRect(snakeState.snakeX, snakeState.snakeY, scale, scale);
    ctx.fillStyle = 'red';
    ctx.fillRect(food.foodX, food.foodY, scale, scale);
}


function loop() {
    let speed = 5;

    update(speed);
    draw();

    // 60 times per second = 60FPS = every 16,67 milliseconds
    window.requestAnimationFrame(loop);
}

window.requestAnimationFrame(loop);

let keyMap = {
    81: 'left',
    37: 'left',
    68: 'right',
    39: 'right',
    90: 'up',
    38: 'up',
    83: 'down',
    40: 'down',
};

function keydown(event) {
    let key = keyMap[event.keyCode];
    snakeState.pressedKeys.left = false;
    snakeState.pressedKeys.right= false;
    snakeState.pressedKeys.up   = false;
    snakeState.pressedKeys.down = false;
    snakeState.pressedKeys[key] = true;
}

window.addEventListener("keydown", keydown, false);

